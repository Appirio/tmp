const fs = require('fs');
const path = require('path');
const os = require('os');

// Monkey Patch Start => resolve-dependencies
// ----------------------
const patchDir = path.dirname(require.resolve('resolve-dependencies'));
const patchFile = path.join(patchDir, 'node-loader.js');

let contents = fs.readFileSync(patchFile, 'utf8');
contents = contents.replace(`file.absPath = absPath;`, `const sanePath = absPath.replace('\u0000', '');\n    file.absPath = sanePath;`);
contents = contents.replace(`file.contents = readFileSync(absPath, 'utf-8');`, `file.contents = readFileSync(sanePath, 'utf-8');`);

fs.writeFileSync(patchFile, contents, 'utf8');
// ----------------------
// Monkey Patch End => resolve-dependencies

const nexe = require('nexe');

nexe.compile({
  build: true,
  input: './index.js',
  target: '14.15.4',
  name: 'adx',
  output: `${os.homedir()}/adx-build/adx14`,
  make: ['-j4'],
  loglevel: 'verbose'
});
